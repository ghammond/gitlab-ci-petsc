#include <stdio.h>

#include "petscsys.h"

int main(int argc,char* argv[]){

  PetscMPIInt rank;
  PetscMPIInt size;

  PetscInitialize(&argc,&argv,(char *)0,(char *)0);
  MPI_Comm_size(PETSC_COMM_WORLD, &size);
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  if (!rank) printf("Beginning of C test program\n");

  if (!rank) printf("End of C test program\n");

  MPI_Finalize();

}
