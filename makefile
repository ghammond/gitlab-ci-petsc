# $Id: makefile,v 1.3 2004/07/31 01:16:44 lichtner Exp $

objdir = ./${PETSC_ARCH}/obj
srcdir = ./
pflotran_src = ./
common_src   = ./
bindir = ./${PETSC_ARCH}/bin

MYFLAGS          =

CFLAGS		 = 
FFLAGS		 =
CPPFLAGS         = ${MYFLAGS}
FPPFLAGS         = ${MYFLAGS}

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
#include ${PETSC_DIR}/lib/petsc-conf/variables
#include ${PETSC_DIR}/lib/petsc-conf/rules
#include ${PETSC_DIR}/conf/variables
#include ${PETSC_DIR}/conf/rules

petsc_test_c: petsc_test_c.o
	${CLINKER}   -o petsc_test_c petsc_test_c.o ${PETSC_LIB} ${LIBS} 

all: petsc_test_c
